package es.ujaen.ssccdd;

public class ProblemasException extends Exception {
    private final String duendeID;

    public ProblemasException(String message, String duendeID) {
        super(message);
        this.duendeID = duendeID;
    }

    public String getDuendeID() {
        return duendeID;
    }
}
